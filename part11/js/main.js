$(document).ready(function(){
  $('button').click(function(){
    $('p').css({'background': 'red', 'color': 'white'})
    .slideUp(2000)
    .slideDown(2000, function(){
      $('p').css({'background': 'white', 'color': '#000'})
    });
  });
});
